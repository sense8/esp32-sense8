# ESP32 - sense8

Playing with esp32 and sensors breakout.

Entry point [main.cpp](./src/main.cpp)

* see [platformio.ini](platformio.ini)

```ini
// for main code
build_flags = -D SENSE8_MAIN
```

```ini
// for test code
build_flags = -D SENSE8_TEST_I2C
```

## Board

* ESP32­WROOM­32D
* bme680 breakout (i2c)

### I2C

see [main.cpp](./src/main.cpp)

```cpp
#define BUS_I2C_SDA 16
#define BUS_I2C_SCL 17
#define BUS_I2C_ADDRESS_BME 0x76
```

## Install

Install [vscode](https://code.visualstudio.com/).
Install [platformio](https://platformio.org/platformio-ide) plugin.
Open the project.
Plug your __esp32__ dev board on _USB_.
Click on __platformio__ icon, clik `Upload and Monitor`.

## Links

* [FreeRTOS - queue](https://freertos.org/a00018.html)
* [FreeRTOS - task](https://www.freertos.org/implementing-a-FreeRTOS-task.html)
* [platformio - project configuration](https://docs.platformio.org/en/latest/projectconf/section_env.html#projectconf-section-env)
* [Writing a Library for Arduino](https://www.arduino.cc/en/Hacking/LibraryTutorial)
* [Digital output experiment using an LED](https://techexplorations.com/guides/esp32/begin/digitalout/)
* [esp32-bme680-sensor-arduino](https://randomnerdtutorials.com/esp32-bme680-sensor-arduino/)
* PDF [environmental-sensing-service-1-0](https://www.bluetooth.com/specifications/specs/environmental-sensing-service-1-0/)
* PDF [assigned-values/16-bit-UUID-Numbers-Document.pdf](https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf)