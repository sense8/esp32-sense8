/*
  Morse.h - Library for flashing Morse code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef MORSE_H
#define MORSE_H

#include "Arduino.h"

class Morse
{
public:
  Morse(int pin);
  void dot();
  void dash();

private:
  int _pin;
};

#endif
