/*
  Morse.cpp - Library for flashing Morse code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/

#include "Arduino.h"
#include "Morse.h"

#define MORSE_DOT_DELAY 250
#define MORSE_DASH_DELAY 500
#define MORSE_BETWEEN_DELAY MORSE_DOT_DELAY

Morse::Morse(int pin)
{
    pinMode(pin, OUTPUT);
    _pin = pin;
}

void Morse::dot()
{
    digitalWrite(_pin, HIGH);
    delay(MORSE_DOT_DELAY);
    digitalWrite(_pin, LOW);
    delay(MORSE_BETWEEN_DELAY);
}

void Morse::dash()
{
    digitalWrite(_pin, HIGH);
    delay(MORSE_DASH_DELAY);
    digitalWrite(_pin, LOW);
    delay(MORSE_BETWEEN_DELAY);
}
