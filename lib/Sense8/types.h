#ifndef SENSE8_TYPES_H
#define SENSE8_TYPES_H

#define SERIAL_BAUDRATE 112500
// #define SERIAL_BAUDRATE 9600
#define SERIAL_DELAY 333

// Commenting disable serial logging

#ifdef SENSE8_DEBUG
#define SENSE8_LOGGER_EXTERN extern Logger logger;
#define SENSE8_LOGGER auto logger = Logger(SENSE8_DEBUG);
#define LOG(val) logger.print(val);
#define LOGLN(val) logger.println(val);
#define LOGF(fmt, ...) Serial.printf(fmt, __VA_ARGS__);
#else
#define LOG(msg) ;
#define LOGLN(msg) ;
#define LOGF(fmt, val...) ;
#define SENSE8_LOGGER ;
#define SENSE8_LOGGER_EXTERN ;
#endif

#endif
