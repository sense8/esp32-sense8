#ifndef SENSE8_QUEUE_H
#define SENSE8_QUEUE_H

#include "FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "Logger.h"
#include "types.h"
#include "utils.h"

#define QUEUE_WAIT_MS portTICK_PERIOD_MS * 100

class Queue
{
public:
    Queue(uint, uint);
    ~Queue();
    virtual BaseType_t init();
    BaseType_t push(void *);
    BaseType_t push(void *, int);
    BaseType_t pop(void *);
    BaseType_t pop(void *, int);
    BaseType_t peek(void *, int);
    int size();

private:
    QueueHandle_t _queue;
    uint _size;
    uint _desiredQueueLength;
    uint _itemSize;
};

#endif