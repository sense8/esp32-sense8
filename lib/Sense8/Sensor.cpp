#include "Sensor.h"

SENSE8_LOGGER_EXTERN

void makeSensorBus(SensorBus *bus, int address = 77, int sda = 21, int scl = 22)
{
    bus->i2c.address = address;
    bus->i2c.sda = sda;
    bus->i2c.scl = scl;
}

Sensor::Sensor()
{
    makeSensorBus(&_bus);
}

BaseType_t Sensor::read(void *data)
{
    Wire.begin(_bus.i2c.sda, _bus.i2c.scl, 0);
    return read_bus(data);
}

BaseType_t Sensor::setup()
{
    Wire.begin(_bus.i2c.sda, _bus.i2c.scl, 0);
    return pdTRUE;
}

BaseType_t Sensor::read_bus(void *)
{
    return pdTRUE;
}

SensorBus *Sensor::getBus()
{
    return &_bus;
}