#ifndef SENSE8_SENSOR_BME280_H
#define SENSE8_SENSOR_BME280_H

#include <Wire.h>
#include "Logger.h"
#include "types.h"
#include "Sensor.h"
#include "I2C.h"
#include <Adafruit_Sensor.h>
#include "Adafruit_BME280.h"

#define SEALEVELPRESSURE_HPA (1013.25)

struct SensorDataBME280
{
    // °C
    float temperature;
    // RH %
    float humidity;
    // hPa ( / 100)
    uint32_t pressure;
    // m
    float altitude;
};

class BME280Sensor : public Sensor
{
public:
    BME280Sensor();
    ~BME280Sensor();
    BaseType_t setup();
    BaseType_t read_bus(void *);
    void toString(char *);

private:
    Adafruit_BME280 _sensor; // I2C
};
#endif
