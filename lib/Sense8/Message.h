#ifndef SENSE8_MESSAGE_H
#define SENSE8_MESSAGE_H

#include "Queue.h"

enum class MessageKind
{
    debug = 2,
    sensor,
    admin
};

enum class MessageSubKind
{
    mock = 2,
    bme680,
    as726x
};

struct SensorEnvironmentMessage
{
    float temperature;
    float humidity;
    uint32_t pressure;
    uint32_t gas_resistance;
    float altitude;
};

struct Envelope
{
    MessageKind kind;
    MessageSubKind subKind;
    void *message;
};

class MessageQueue : public Queue
{
public:
    MessageQueue(int queueLength);
};

#endif