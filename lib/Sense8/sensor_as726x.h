#ifndef SENSE8_SENSOR_AS726X_H
#define SENSE8_SENSOR_AS726X_H

#include <Wire.h>
#include "Logger.h"
#include "types.h"
#include "Sensor.h"
#include "I2C.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_AS726x.h>

struct SensorDataAS726x
{
    float violet;
    float blue;
    float green;
    float yellow;
    float orange;
    float red;
};

class AS726xSensor : public Sensor
{
public:
    AS726xSensor();
    ~AS726xSensor();
    BaseType_t setup();
    BaseType_t read_bus(void *);
    void toString(char *);

private:
    Adafruit_AS726x _sensor; // I2C
};
#endif
