#ifndef SENSE8_MORSE_TASK_H
#define SENSE8_MORSE_TASK_H

#include "Task.h"
#include <Morse.h>

struct morse_stack_t
{
    int pin{};
};

class MorseTask : public Task
{

public:
    MorseTask(int pin);
    void run(void *);
    void stop();
    int pin();

protected:
    int _pin;
    morse_stack_t mt;
};

#endif
