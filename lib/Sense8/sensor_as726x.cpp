#include "sensor_as726x.h"

SENSE8_LOGGER_EXTERN

AS726xSensor::AS726xSensor()
    : Sensor()
{
}

AS726xSensor::~AS726xSensor()
{
}

BaseType_t AS726xSensor::setup()
{
    Sensor::setup();
    Wire.begin(_bus.i2c.sda, _bus.i2c.scl, 0);
    if (!_sensor.begin(&Wire))
    {
        return pdFALSE;
    }
    return pdTRUE;
}

BaseType_t AS726xSensor::read_bus(void *_data)
{
    _sensor.startMeasurement();
    bool rdy = false;
    while (!rdy)
    {
        delay(5);
        rdy = _sensor.dataReady();
    }
    float sensorValues[AS726x_NUM_CHANNELS];
    _sensor.readCalibratedValues(sensorValues);
    // _sensor.readRawValues(sensorValues);
    auto data = (SensorDataAS726x *)_data;
    data->blue = sensorValues[AS726x_BLUE];
    data->green = sensorValues[AS726x_GREEN];
    data->orange = sensorValues[AS726x_ORANGE];
    data->red = sensorValues[AS726x_RED];
    data->violet = sensorValues[AS726x_VIOLET];
    data->yellow = sensorValues[AS726x_YELLOW];
    return pdTRUE;
}