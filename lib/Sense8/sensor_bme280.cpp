#include "sensor_bme280.h"

SENSE8_LOGGER_EXTERN

BME280Sensor::BME280Sensor()
    : Sensor()
{
}

BME280Sensor::~BME280Sensor()
{
}

BaseType_t BME280Sensor::setup()
{
    Sensor::setup();
    if (!_sensor.begin(_bus.i2c.address))
    {
        return pdFALSE;
    }
    // Set up oversampling and filter initialization
    _sensor.setSampling();
    return pdTRUE;
}

BaseType_t BME280Sensor::read_bus(void *_data)
{
    auto data = (SensorDataBME280 *)_data;
    data->temperature = _sensor.readTemperature();
    data->pressure = _sensor.readPressure();
    data->humidity = _sensor.readHumidity();
    data->altitude = _sensor.readAltitude(SEALEVELPRESSURE_HPA);
    return pdTRUE;
}