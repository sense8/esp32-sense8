#ifndef SENSE8_SERVICE_MOCK_H
#define SENSE8_SERVICE_MOCK_H

#include "Service.h"
#include "Logger.h"

class MockService : public Service
{
public:
    MockService() : Service("mock"){};
};

#endif
