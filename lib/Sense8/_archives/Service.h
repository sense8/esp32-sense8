#ifndef SENSE8_SERVICE_H
#define SENSE8_SERVICE_H

#include "FreeRTOS.h"
#include "freertos/task.h"
#include "Logger.h"
#include "types.h"

enum class ServiceStatus
{
    STOPPED,
    STARTED
};

class Service
{

public:
    Service(const char *);
    const char *get_name();
    void start();
    void stop();
    void restart();

protected:
    void task(void *pvParameters);
    const char *name;
    ServiceStatus status = ServiceStatus::STOPPED;
    TaskHandle_t xHandle = nullptr;
};

#endif
