#ifndef SENSE8_SERVICE_MANAGER_H
#define SENSE8_SERVICE_MANAGER_H

#include "Service.h"
#include "Logger.h"
#include "types.h"

class ServiceManager
{
public:
    ServiceManager();
    bool add(Service *);

private:
    // Service services[];
};

#endif