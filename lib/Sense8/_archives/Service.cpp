#include "Service.h"

SENSE8_LOGGER_EXTERN

Service::Service(const char *_name)
{
    name = _name;
}

const char *Service::get_name()
{
    return name;
}

void Service::start()
{
    LOGF("starting task %s\n", get_name());
}

void Service::stop()
{
    LOGF("stopping task %s\n", get_name());
}

void Service::restart()
{
    stop();
    start();
}
