#include "Task.h"

SENSE8_LOGGER_EXTERN

Task::Task(std::string name, f_task_runnable runnable)
    : _parameters(
          TaskParameters{
              name,
              TaskQueues{
                  MessageQueue(TASK_QUEUE_SIZE),
                  MessageQueue(TASK_QUEUE_SIZE)},
              nullptr}),
      _runnable(runnable)
{
    ;
}

Task::Task(std::string name, f_task_runnable runnable, TaskQueues *queues)
    : _parameters(
          TaskParameters{
              name,
              *queues,
              nullptr}),
      _runnable(runnable)
{
    ;
}

BaseType_t Task::init()
{
    if (pdFALSE == _parameters.queues.in.init())
    {
        return pdFALSE;
    }
    if (pdFALSE == _parameters.queues.out.init())
    {
        return pdFALSE;
    }
    return pdTRUE;
}

BaseType_t Task::run(void *pvParameters)
{
    _parameters.pvParameters = pvParameters;
    xTaskCreate(_runnable, _parameters.name.c_str(), 8192, &_parameters, tskIDLE_PRIORITY, &_xHandle);
    if (NULL == _xHandle)
    {
        return pdFALSE;
    }
    return pdTRUE;
}

void Task::stop()
{
    if (NULL == _xHandle)
    {
        return;
    }
    // struct Envelope msg =
    //     {
    //         .kind = MessageKind::debug,
    //         .subKind = MessageSubKind::bme680,
    //         .message = new TaskMessage{TaskCommand::stop}};
    // _parameters.queues.in.push(&msg);
    // delay(1000);
    vTaskDelete(_xHandle);
    _xHandle = NULL;
}

TaskParameters *Task::getParameters()
{
    return &_parameters;
}