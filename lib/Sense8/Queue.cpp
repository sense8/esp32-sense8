#include "Queue.h"

SENSE8_LOGGER_EXTERN

Queue::Queue(uint queueLength, uint itemSize)
    : _queue(NULL),
      _size(0),
      _desiredQueueLength(queueLength),
      _itemSize(itemSize)
{
    ;
}

Queue::~Queue()
{
    _queue = NULL;
}

BaseType_t Queue::init()
{
    if (NULL != _queue)
    {
        LOGLN("warning: queue already initialized");
        return pdTRUE;
    }
    _queue = xQueueCreate(_desiredQueueLength, _itemSize);
    configASSERT(_queue);
    if (NULL == _queue)
    {
        return pdFALSE;
    }
    return pdTRUE;
}

BaseType_t Queue::push(void *item)
{
    return push(item, portMAX_DELAY);
}

BaseType_t Queue::push(void *item, int ticksToWait)
{
    auto res = xQueueSend(_queue, item, ticksToWait);
    if (pdTRUE != res)
    {
        return res;
    }
    _size += 1;
    return pdTRUE;
}

BaseType_t Queue::pop(void *item)
{
    return pop(item, portMAX_DELAY);
}

BaseType_t Queue::pop(void *item, int ticksToWait)
{
    auto res = xQueueReceive(_queue, item, ticksToWait);
    if (pdTRUE != res)
    {
        return pdFALSE;
    }
    _size -= 1;
    return pdTRUE;
}

BaseType_t Queue::peek(void *item, int ticksToWait)
{
    auto res = xQueuePeek(_queue, item, ticksToWait);
    if (pdTRUE != res)
    {
        return pdFALSE;
    }
    return pdTRUE;
}

int Queue::size()
{
    return _size;
}