#ifndef SENSE8_UTILS_H
#define SENSE8_UTILS_H

#include <sstream>
#include <iostream>

std::string int2str(int input);
const char *floatToHex(const float value);
const char *uintToHex(const uint32_t value);
#endif