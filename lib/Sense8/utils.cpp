#include "utils.h"
#include <sstream>
#include <iomanip>
#include "Arduino.h"

// convert int to char*
std::string int2str(int input)
{
    std::stringstream ss;
    ss << input;
    return ss.str();
}

const char *floatToHex(const float value)
{
    std::stringstream ss;
    ss << std::hexfloat << value;
    return ss.str().c_str();
}

const char *uintToHex(const uint32_t value)
{
    std::stringstream ss;
    ss << std::hex << value;
    return ss.str().c_str();
}
