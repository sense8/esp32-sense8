#ifndef SENSE8_SENSOR_BME680_H
#define SENSE8_SENSOR_BME680_H

#include <Wire.h>
#include "Logger.h"
#include "types.h"
#include "Sensor.h"
#include "I2C.h"
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define SEALEVELPRESSURE_HPA (1013.25)

struct SensorDataBME680
{
    // °C
    float temperature;
    // RH %
    float humidity;
    // hPa ( / 100)
    uint32_t pressure;
    // KOhms ( / 1000)
    uint32_t gas_resistance;
    // m
    float altitude;
};

class BME680Sensor : public Sensor
{
public:
    BME680Sensor();
    ~BME680Sensor();
    BaseType_t setup();
    BaseType_t read_bus(void *);
    void toString(char *);

private:
    Adafruit_BME680 _sensor; // I2C
};
#endif
