#ifndef SENSE8_TASK_H
#define SENSE8_TASK_H

#include "FreeRTOS.h"
#include "freertos/task.h"
#include "Logger.h"
#include "types.h"
#include "Message.h"

using f_task_runnable = void (*)(void *);

#define TASK_QUEUE_SIZE 1

struct TaskQueues
{
    MessageQueue in;
    MessageQueue out;
};

struct TaskParameters
{
    std::string name;
    TaskQueues queues;
    void *pvParameters;
};

enum class TaskCommand
{
    stop
};

struct TaskMessage
{
    TaskCommand command;
};

class Task
{

public:
    Task(std::string, f_task_runnable runnable);
    Task(std::string, f_task_runnable runnable, TaskQueues *queues);
    BaseType_t init();
    BaseType_t run(void *pvParameters);
    TaskParameters *getParameters();
    void stop();

protected:
    TaskParameters _parameters;
    f_task_runnable _runnable;
    TaskHandle_t _xHandle = NULL;
};

#endif
