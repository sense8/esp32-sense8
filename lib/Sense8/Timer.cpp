#include "Timer.h"

SENSE8_LOGGER_EXTERN

Timer::Timer(const char *pcTimerName,
             TickType_t xTimerPeriodInTicks,
             UBaseType_t uxAutoReload,
             TimerCallbackFunction_t pxCallbackFunction)
    : _pcTimerName(pcTimerName),
      _xTimerPeriodInTicks(xTimerPeriodInTicks),
      _uxAutoReload(uxAutoReload),
      _pxCallbackFunction(pxCallbackFunction),
      _pvTimerID((void *)0),
      _xTicksToWait(0)
{
    ;
}

Timer::~Timer()
{
    if (NULL != _handle)
    {
        xTimerDelete(_handle, _xTicksToWait);
        LOG("DELETE TIMER");
        _handle = NULL;
    }
    if (NULL != _pxCallbackFunction)
    {
        _pxCallbackFunction = NULL;
    }
}

BaseType_t Timer::init()
{
    if (NULL != _handle)
    {
        return pdTRUE;
    }
    _handle = xTimerCreate(_pcTimerName, _xTimerPeriodInTicks, _uxAutoReload, _pvTimerID, _pxCallbackFunction);
    if (NULL == _handle)
    {
        return pdFALSE;
    }
    return pdTRUE;
}

BaseType_t Timer::start()
{
    auto ret = init();
    if (pdTRUE != ret)
    {
        return ret;
    }
    return xTimerStart(_handle, _xTicksToWait);
}

BaseType_t Timer::reset()
{
    if (NULL == _handle)
    {
        return pdFALSE;
    }
    return xTimerReset(_handle, _xTicksToWait);
}

void Timer::setTicksToWait(TickType_t xTicksToWait)
{
    _xTicksToWait = xTicksToWait;
}