#ifndef SENSE8_I2C_H
#define SENSE8_I2C_H

#include <functional>
#include <Wire.h>
#include "Logger.h"
#include "types.h"

using std::function;

typedef int i2c_line_t;

class I2c
{
public:
    I2c(i2c_line_t sda, i2c_line_t scl);
    void scan();

private:
    i2c_line_t _sda;
    i2c_line_t _scl;
};
#endif
