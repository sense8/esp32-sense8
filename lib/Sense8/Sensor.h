#ifndef SENSE8_SENSOR_H
#define SENSE8_SENSOR_H

#include <functional>
#include <Wire.h>
#include "Logger.h"
#include "types.h"

using std::function;

struct SensorBusI2C
{
    int address;
    int scl;
    int sda;
};

struct SensorBus
{
    SensorBusI2C i2c;
};

void makeSensorBus(SensorBus *, int, int, int);

class Sensor
{
public:
    Sensor();
    virtual ~Sensor(){};
    BaseType_t read(void *);
    SensorBus *getBus();
    virtual BaseType_t setup();
    virtual BaseType_t read_bus(void *);

protected:
    SensorBus _bus;
};
#endif
