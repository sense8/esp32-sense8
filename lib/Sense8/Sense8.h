#ifndef SENSE8_H
#define SENSE8_H

#include "Arduino.h"

class Sense8
{
public:
  Sense8(bool);
  Sense8 configure(bool);
  String version();
  bool debug();

private:
  String _version;
  bool _debug;
};

#endif
