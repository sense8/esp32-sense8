#ifndef SENSE8_LOGGER_H
#define SENSE8_LOGGER_H

#include "types.h"
#include <stdarg.h>
#include "Arduino.h"

class Logger
{
public:
  Logger(bool);
  void configure(bool);
  void print(const char *);
  void println(const char *);
  void printf(const char *fmt, ...);

protected:
  bool _enabled;
};

#endif
