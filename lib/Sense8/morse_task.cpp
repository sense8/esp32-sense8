#include "morse_task.h"

SENSE8_LOGGER_EXTERN

void morse_task(void *ptr)
{
    auto ms = (morse_stack_t *)ptr;
    auto m = Morse(ms->pin);
    for (;;)
    {
        m.dot();
        m.dot();
        m.dot();
        m.dash();
        m.dash();
        m.dash();
        m.dot();
        m.dot();
        m.dot();
        delay(SERIAL_DELAY);
    }
}

MorseTask::MorseTask(int pin)
    : Task("morse", morse_task),
      _pin(pin)
{
    mt.pin = _pin;
}

void MorseTask::run(void *p)
{
    Task::run(&mt);
    /* @TODO If not set, only last pin will work.
    Need lock primitive to access pin ?
    */
    delay(1);
}

void MorseTask::stop()
{
    Task::stop();
}

int MorseTask::pin()
{
    return _pin;
}