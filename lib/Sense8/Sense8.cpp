#include "Sense8.h"

Sense8::Sense8(bool debug)
{
    _debug = false;
    _version = "0.0.1";
    configure(debug);
}

Sense8 Sense8::configure(bool debug = true)
{

    return this;
}

String Sense8::version()
{
    return _version;
}

bool Sense8::debug()
{
    return _debug;
}
