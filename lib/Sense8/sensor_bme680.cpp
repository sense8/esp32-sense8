#include "sensor_bme680.h"

SENSE8_LOGGER_EXTERN

BME680Sensor::BME680Sensor()
    : Sensor()
{
}

BME680Sensor::~BME680Sensor()
{
}

BaseType_t BME680Sensor::setup()
{
    Sensor::setup();
    if (!_sensor.begin(_bus.i2c.address))
    {
        return pdFALSE;
    }
    // Set up oversampling and filter initialization
    _sensor.setTemperatureOversampling(BME680_OS_8X);
    _sensor.setHumidityOversampling(BME680_OS_4X);
    _sensor.setPressureOversampling(BME680_OS_4X);
    _sensor.setIIRFilterSize(BME680_FILTER_SIZE_3);
    // _sensor.setGasHeater(320, 150); // 320*C for 150 ms
    return pdTRUE;
}

BaseType_t BME680Sensor::read_bus(void *_data)
{
    auto data = (SensorDataBME680 *)_data;
    unsigned long endTime = _sensor.beginReading();
    if (0 == endTime)
    {
        return pdFALSE;
    }
    if (!_sensor.endReading())
    {
        return pdFALSE;
    }
    data->temperature = _sensor.temperature;
    data->pressure = _sensor.pressure;
    data->humidity = _sensor.humidity;
    data->gas_resistance = _sensor.gas_resistance;
    data->altitude = _sensor.readAltitude(SEALEVELPRESSURE_HPA);
    return pdTRUE;
}