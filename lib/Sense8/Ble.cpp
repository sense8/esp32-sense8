#include "Ble.h"

SENSE8_LOGGER_EXTERN

BLE_Characteristic BLEC_Temperature = {
    name : "temperature",
    uuid : "2A6E",
    properties : BLECharacteristic::PROPERTY_READ,
    pCharacteristic : nullptr
};

BLE_Characteristic BLEC_Humidity = {
    name : "humidity",
    uuid : "2A6F",
    properties : BLECharacteristic::PROPERTY_READ,
    pCharacteristic : nullptr
};

BLE_Characteristic BLEC_Pressure = {
    name : "pressure",
    uuid : "2A6D",
    properties : BLECharacteristic::PROPERTY_READ,
    pCharacteristic : nullptr
};

// No defined UUID in spec for gaz resistance
BLE_Characteristic BLEC_GazResistance = {
    name : "gaz_resistance",
    uuid : "ce1a7980-e6cd-47f6-ba61-247ac04b8086",
    properties : BLECharacteristic::PROPERTY_READ,
    pCharacteristic : nullptr
};

BLE_Characteristic BLEC_Altitude = {
    name : "altitude",
    uuid : "2AB3",
    properties : BLECharacteristic::PROPERTY_READ,
    pCharacteristic : nullptr
};

BLE_Characteristic BLE_CHARACTERISTICS[] = {
    BLEC_Temperature,
    BLEC_Humidity,
    BLEC_Pressure,
    BLEC_GazResistance,
    BLEC_Altitude};

#define IDX_TEMPERATURE 0
#define IDX_HUMIDITY 1
#define IDX_PRESSURE 2
#define IDX_GAZ_RESISTANCE 3
#define IDX_ALTITUDE 4

Ble::Ble()
    : _pServer(nullptr),
      _pService(nullptr),
      _status(BleStatus::stopped)
{
    ;
}

Ble::~Ble()
{
    stop();
}

BaseType_t Ble::start(const char *name)
{
    BLEDevice::init(name);
    if (nullptr != _pServer || nullptr != _pService)
    {
        return pdFALSE;
    }
    _pServer = BLEDevice::createServer();
    _pService = _pServer->createService(SERVICE_UUID);
    for (auto c : BLE_CHARACTERISTICS)
    {
        c.pCharacteristic = _pService->createCharacteristic(
            c.uuid, c.properties);
    }
    _pService->start();
    _status = BleStatus::started;
    return pdTRUE;
}

BaseType_t Ble::stop()
{
    _status = BleStatus::stopped;
    if (_pService)
    {
        _pService->stop();
        _pService = nullptr;
    }
    if (_pServer)
    {
        _pServer->disconnect(_pServer->getConnId());
        _pServer = nullptr;
    }
    BLEDevice::deinit(true);
    return pdTRUE;
}

BaseType_t Ble::startAdvertising(SensorEnvironmentMessage *sensor)
{
    setEnvironment(sensor);
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
    _status = BleStatus::advertising;
    LOGLN("[ble] characteristics defined!");
    return pdTRUE;
}

BaseType_t Ble::setEnvironment(SensorEnvironmentMessage *sensor)
{
    auto c = _pService->getCharacteristic(BLE_CHARACTERISTICS[IDX_TEMPERATURE].uuid);
    c->setValue(floatToHex(sensor->temperature));
    c = _pService->getCharacteristic(BLE_CHARACTERISTICS[IDX_HUMIDITY].uuid);
    c->setValue(floatToHex(sensor->humidity));
    c = _pService->getCharacteristic(BLE_CHARACTERISTICS[IDX_PRESSURE].uuid);
    c->setValue(uintToHex(sensor->pressure));
    c = _pService->getCharacteristic(BLE_CHARACTERISTICS[IDX_GAZ_RESISTANCE].uuid);
    c->setValue(uintToHex(sensor->gas_resistance));
    c = _pService->getCharacteristic(BLE_CHARACTERISTICS[IDX_ALTITUDE].uuid);
    c->setValue(uintToHex(sensor->altitude));
    return pdTRUE;
}

BleStatus Ble::status()
{
    return _status;
}