#include "Message.h"

auto messageSize = sizeof(struct Envelope);

MessageQueue::MessageQueue(int queueLength)
    : Queue(queueLength, messageSize) { init(); };
