#ifndef SENSE8_BLE_H
#define SENSE8_BLE_H

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#include "Logger.h"
#include "types.h"
#include "utils.h"
#include "Message.h"

#define SERVICE_UUID "181A" // Environment Service

struct BLE_Characteristic
{
    std::string name;
    std::string uuid;
    uint32_t properties;

    BLECharacteristic *pCharacteristic;
};

enum class BleStatus
{
    stopped,
    started,
    advertising,
};

class Ble
{
public:
    Ble();
    ~Ble();
    BaseType_t start(const char *);
    BaseType_t setEnvironment(SensorEnvironmentMessage *);
    BaseType_t startAdvertising(SensorEnvironmentMessage *);
    BleStatus status();
    BaseType_t stop();

private:
    BLEServer *_pServer;
    BLEService *_pService;
    BleStatus _status;
};
#endif
