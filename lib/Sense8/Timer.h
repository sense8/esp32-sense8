#ifndef SENSE8_TIMER_H
#define SENSE8_TIMER_H
#define configTIMER_QUEUE_LENGTH 10

#include "FreeRTOS.h"
#include "freertos/timers.h"
#include "Logger.h"
#include "types.h"

class Timer
{
public:
    Timer(const char *pcTimerName,
          TickType_t xTimerPeriodInTicks,
          UBaseType_t uxAutoReload,
          TimerCallbackFunction_t pxCallbackFunction);
    virtual ~Timer();
    BaseType_t init();
    BaseType_t start();
    BaseType_t reset();
    void setTicksToWait(TickType_t);

private:
    const char *_pcTimerName;
    TimerHandle_t _handle;
    TickType_t _xTimerPeriodInTicks;
    UBaseType_t _uxAutoReload;
    TimerCallbackFunction_t _pxCallbackFunction;
    void *_pvTimerID;
    TickType_t _xTicksToWait;
};
#endif