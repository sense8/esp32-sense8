#include "Logger.h"

Logger::Logger(bool enabled = false)
{
    _enabled = false;
    configure(enabled);
}

void Logger::configure(bool enabled = true)
{
    if (enabled == _enabled)
    {
        return;
    }
    _enabled = enabled;
    if (_enabled)
    {
#ifdef SENSE8_DEBUG
        Serial.begin(SERIAL_BAUDRATE);
        delay(10);
        println("[serial] on");
#endif
    }
    else
    {
#ifdef SENSE8_DEBUG
        Serial.end();
#endif
    }
    return;
}

void Logger::println(const char *val)
{
#ifdef SENSE8_DEBUG
    Serial.println(val);
#endif
}

void Logger::printf(const char *fmt, ...)
{
#ifdef SENSE8_DEBUG
#endif
}

void Logger::print(const char *val)
{
#ifdef SENSE8_DEBUG
    Serial.print(val);
#endif
}