#include "I2C.h"

SENSE8_LOGGER_EXTERN

I2c::I2c(i2c_line_t sda, i2c_line_t scl)
    : _sda(sda), _scl(scl)
{
}

// Should use callback for each device found
void I2c::scan()
{
    int error, address;
    int nDevices;
    Serial.printf("[i2c] Scanning sda: 0x%i, scl: 0x%i\n", _sda, _scl);
    nDevices = 0;
    Wire.begin(_sda, _scl, 0);
    for (address = 1; address < 127; address++)
    {
        Wire.beginTransmission(address);
        error = Wire.endTransmission();
        if (error == 0)
        {
            Serial.print("[i2c] device found at address 0x");
            if (address < 16)
            {
                Serial.print("0");
            }
            Serial.println(address, HEX);
            nDevices++;
        }
        else if (error == 4)
        {
            Serial.print("[i2c] Unknow error at address 0x");
            if (address < 16)
            {
                Serial.print("0");
            }
            Serial.println(address, HEX);
        }
    }
    if (nDevices == 0)
    {
        Serial.println("[i2c] No I2C devices found");
    }
    else
    {
        Serial.println("done");
    }
    delay(5000);
}