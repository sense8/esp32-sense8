#ifdef SENSE8_TEST_I2C

#include "sensor_bme680.h"
#include "sensor_bme280.h"
#include "sensor_as726x.h"
#include "I2C.h"

#define BUS_I2C_SDA 16
#define BUS_I2C_SCL 17
#define BUS_I2C_ADDRESS_BME 0x76
#define BUS_I2C_ADDRESS_AS 0x49
#define MSEC_TO_SEC 1000000

SENSE8_LOGGER

BME280Sensor sensor_bme;
AS726xSensor sensor_as;
bool SETUP_OK = true;

void setup()
{

    auto bus = sensor_bme.getBus();
    bus->i2c.sda = BUS_I2C_SDA;
    bus->i2c.scl = BUS_I2C_SCL;
    bus->i2c.address = BUS_I2C_ADDRESS_BME;
    if (pdTRUE != sensor_bme.setup())
    {
        LOG("[bme] SensorSetupError");
        SETUP_OK = false;
        return;
    }
    // bus = sensor_as.getBus();
    // bus->i2c.sda = BUS_I2C_SDA;
    // bus->i2c.scl = BUS_I2C_SCL;
    // bus->i2c.address = BUS_I2C_ADDRESS_AS;
    // if (pdTRUE != sensor_as.setup())
    // {
    //     LOG("[as] SensorSetupError");
    //     SETUP_OK = false;
    //     return;
    // }
}

void loop()
{
    if (!SETUP_OK)
    {
        I2c i2cBus(BUS_I2C_SDA, BUS_I2C_SCL);
        i2cBus.scan();
        return;
    }
    SensorDataBME280 data_bme;
    if (pdTRUE != sensor_bme.read(&data_bme))
    {
        Serial.println("[bme] Reading fail");
        delay(2000);
        return;
    }
    else
    {
        Serial.println("[bme280]");
        Serial.printf("temperature: %f °C\n", data_bme.temperature);
        Serial.printf("humidity: %f %\n", data_bme.humidity);
        Serial.printf("pressure: %f hPa\n", data_bme.pressure / 100.0);
        Serial.printf("altitude: %f m\n", data_bme.altitude);
        // Serial.printf("gaz resistance: %f hPa\n", data_bme.gas_resistance / 1000.0);
    }

    // SensorDataAS726x data_as;
    // if (pdTRUE != sensor_as.read(&data_as))
    // {
    //     Serial.println("[as] Reading fail");
    //     delay(2000);
    //     return;
    // }
    // else
    // {
    //     Serial.println("[as7262]");
    //     Serial.printf("violet: %f\n", data_as.violet);
    //     Serial.printf("blue: %f\n", data_as.blue);
    //     Serial.printf("green: %f\n", data_as.green);
    //     Serial.printf("yellow: %f\n", data_as.yellow);
    //     Serial.printf("orange: %f\n", data_as.orange);
    //     Serial.printf("red: %f\n", data_as.red);
    // }
    esp_deep_sleep(10 * MSEC_TO_SEC);
}

#endif
