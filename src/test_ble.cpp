#ifdef SENSE8_TEST_BLE

#include "types.h"
#include "Logger.h"
#include "Ble.h"

SENSE8_LOGGER

Ble ble;

bool SETUP_OK = true;

void setup()
{
    if (pdTRUE != ble.setup("Sense8-Marker"))
    {
        SETUP_OK = false;
    }
}

void loop()
{
    if (!SETUP_OK)
    {
        LOG("Something wrong");
        delay(1000);
        return;
    }
    LOG("LOOP");
    delay(1000);
    ble.start();
    for (;;)
    {
        LOG("alive");
        delay(1000);
    }
}

#endif
