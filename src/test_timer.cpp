#ifdef SENSE8_TEST_TIMER
#include "Timer.h"

SENSE8_LOGGER

std::initializer_list<int> PINS = {32, 33, 25};
auto DIGITAL = LOW;

static int
toggle()
{
    if (DIGITAL)
    {
        DIGITAL = LOW;
    }
    else
    {
        DIGITAL = HIGH;
    }
    return DIGITAL;
}

static void vTimerToggle(xTimerHandle pxTimer)
{
    auto newValue = toggle();
    for (auto pin : PINS)
    {
        digitalWrite(pin, newValue);
    }
}

auto timer = Timer("toggle.LED", 1000, 1, vTimerToggle);

void setup()
{
    auto ret = timer.init();
    if (pdTRUE != ret)
    {
        LOG("Timer init fail");
        return;
    }
    for (auto pin : PINS)
    {
        pinMode(pin, OUTPUT);
    }
}

void loop()
{
    timer.start();
    for (;;)
    {
        delay(3000);
    }
}

#endif
