#ifdef SENSE8_TEST_TASK
#include "types.h"
#include "morse_task.h"

SENSE8_LOGGER

void setup()
{
  ;
}

void loop()
{
  std::initializer_list<MorseTask> tasks = {
      MorseTask(32),
      MorseTask(33),
      MorseTask(25)};
  for (auto task : tasks)
  {
    task.run(nullptr);
  }
  for (;;)
  {
    delay(3000);
  }
}

#endif
