#ifdef SENSE8_MAIN

#include "Message.h"
#include "Queue.h"
#include "Task.h"
#include "utils.h"
#include "Logger.h"
#include "Ble.h"
#include "sensor_bme280.h"

SENSE8_LOGGER

/* I2C sensor
*/
#define BUS_I2C_SDA 16
#define BUS_I2C_SCL 17
#define BUS_I2C_ADDRESS_BME 0x76

#define USEC_TO_SEC 1000000
#define WAIT_COMMAND_SEC 5000 // 5 s
/* Environment Sensing advertising 30s every 5mn.
*/
#define DEEPSLEEP_INTERVAL 30000                     // 30 s
#define DEEPSLEEP_DURATION_USEC USEC_TO_SEC * 60 * 5 // 5 mn

/* Functions
*/
float randfloat(float min, float max)
{
    return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
}

void somethingLoop(const char *something, BaseType_t code)
{
    for (;;)
    {
        LOGF("[%i] %s\n", code, something);
        delay(1000);
    }
}

void sensor_runnable(void *taskParameters)
{
    BME280Sensor sensor_bme;
    auto tp = (TaskParameters *)taskParameters;
    auto queues = (TaskQueues)tp->queues;
    auto bus = sensor_bme.getBus();
    bus->i2c.sda = BUS_I2C_SDA;
    bus->i2c.scl = BUS_I2C_SCL;
    bus->i2c.address = BUS_I2C_ADDRESS_BME;
    if (pdTRUE != sensor_bme.setup())
    {
        somethingLoop("[sensor] setup error", pdFALSE);
    }
    for (;;)
    {
        SensorDataBME280 data_bme;
        if (pdTRUE != sensor_bme.read(&data_bme))
        {
            LOGLN("[bme] Reading fail");
            continue;
        }
        struct Envelope msg = {
            .kind = MessageKind::debug,
            .subKind = MessageSubKind::bme680,
            .message = new SensorEnvironmentMessage{
                data_bme.temperature,
                data_bme.humidity,
                data_bme.pressure,
                0,
                data_bme.altitude}};
        if (pdPASS != queues.out.push(&msg))
        {
            LOGLN("Queue error");
        }
        delay(5000);
    }
}

void ble_runnable(void *taskParameters)
{
    auto tp = (TaskParameters *)taskParameters;
    auto queues = (TaskQueues)tp->queues;
    Ble ble;
    if (pdTRUE != ble.start("Sense8-Marker"))
    {
        somethingLoop("[ble] start error", pdFALSE);
    }
    for (;;)
    {
        auto m = Envelope{};
        if ((pdPASS != queues.out.pop(&m)))
        {
            LOGLN("QueuePopError");
            continue;
        }
        LOGLN("[ble] new sensor data");
        if (ble.status() != BleStatus::advertising)
        {
            ble.startAdvertising((SensorEnvironmentMessage *)m.message);
            continue;
        }
        else
        {
            ble.setEnvironment((SensorEnvironmentMessage *)m.message);
        }
    }
}

/* Exchange data between tasks (queues.in not used)
*/
auto queues = TaskQueues{
    MessageQueue(TASK_QUEUE_SIZE),
    MessageQueue(TASK_QUEUE_SIZE)};

/* FreeRTOS Tasks
*/
Task sensor_task("sensor", sensor_runnable, &queues);
Task ble_task("ble", ble_runnable, &queues);

/* MAIN
*/
auto started_on = millis();
void setup()
{
    ;
}

void loop()
{
    auto elapsed = millis() - started_on;
    sensor_task.run(nullptr);
    ble_task.run(nullptr);
    for (;;)
    {
        elapsed = millis() - started_on;
        if (elapsed < DEEPSLEEP_INTERVAL)
        {
            delay(1000);
            continue;
        }
        ble_task.stop();
        sensor_task.stop();
        LOGF("[main] go deep sleep for %i ms\n", DEEPSLEEP_DURATION_USEC);
        esp_deep_sleep(DEEPSLEEP_DURATION_USEC);
    }
}

#endif
